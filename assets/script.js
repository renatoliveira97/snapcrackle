function snapCrackle(maxValue){

    let myString = "";

    for(let i = 1; i <= maxValue; i++){
        let vImpar = false;
        let vMCinco = false;
        let vPrimo = false;
        let pArray = [];

        if(i % 2 > 0){
            vImpar = true;
        }

        if(i % 5 === 0){
            vMCinco = true;
        }

        for(let a = 2; a <= maxValue; a++){
            if(i % a === 0){
                pArray.push(true);
            } else {
                pArray.push(false);
            }
        }

        let count = 0;

        for(let b = 0; b < pArray.length; b++){
            if(pArray[b] === true){
                count++;
            }
        }

        if(count === 1){
            vPrimo = true;
        }

        if(vImpar === true){
            if(vMCinco === true){
                if(vPrimo === true){
                    myString += "SnapCracklePrime, ";
                } else {
                    myString += "SnapCrackle, ";
                }
            } else if(vPrimo === true){
                myString += "SnapPrime, ";
            } else {
                myString += "Snap, ";
            }
        } else if(vMCinco === true){
            if(vPrimo === true){
                myString += "CracklePrime, ";
            } else {
                myString += "Crackle, ";
            }
        } else if(vPrimo === true){
            myString += "Prime, ";
        } else {
            myString += i + ", ";
        }

    }

    return myString;
}

document.getElementById("SC").onclick = function () { 
    let selectedNumber = prompt("Digite um número positivo de 1 ao infinito!"); 
    alert(snapCrackle(selectedNumber));
    console.log(snapCrackle(selectedNumber));
};

